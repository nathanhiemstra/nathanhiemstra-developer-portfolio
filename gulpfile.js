var gulp =          require('gulp');

// Browser
var browsersync =   require('browser-sync').create();

// CSS
var sass =          require('gulp-sass');
var autoprefixer =  require('gulp-autoprefixer');

// JS
var uglify =        require('gulp-uglify');
var babelify =      require('babelify');
var browserify =    require('browserify');
var source =        require('vinyl-source-stream');
var buffer =        require('vinyl-buffer');


// Utility
var rename  =       require('gulp-rename');
var sourcemaps =    require('gulp-sourcemaps');
var plumber =       require('gulp-plumber');
var cleanFiles =    require('gulp-clean');


var path = {
  server: './dist/',
  src: {
    css:              'src/lib/scss/**/*.scss',
    html:             'src/**/*.html',
    images:           'src/lib/images/**/*.*',
    imagesFolderFiles:'src/lib/images/**/*',
    js:               'src/lib/js/**/*.js',
    jsVendors:        'src/lib/js/vendors/**/*.js',
    jsFolder:         'src/lib/js/',
    jsApp:            'app.js',
    pdfs:             'src/lib/pdfs/**/*.pdf',
  },
  dist: {
    parent:           './dist/',
    files:            './dist/*',
    css:              './dist/lib/css/',
    html:             './dist/',
    images:           './dist/lib/images/',
    js:               './dist/lib/js/',
    jsVendors:        './dist/lib/js/vendors/',
    pdfs:             './dist/lib/pdfs/',
  },
}

function moveFiles( moveSrc, moveDist ) {
  return gulp.src( moveSrc )
    .pipe( gulp.dest( moveDist ) )
    .pipe( browsersync.stream() );
}

var files = {
  js: [ path.src.jsApp ]
}

function clean(done) {
  return gulp.src(path.dist.files, {read: false})
    .pipe(cleanFiles());
  done();
} 

function reload(done) {
  browsersync.reload();
  done();
};

function browser_sync(done) {
  browsersync.init({
    server: {
      baseDir: path.server
    },
    injectChanges: true,
    open: false
  });
  done();
};

function html() {
  return moveFiles(path.src.html, path.dist.html);
};

function pdfs(done) {
  return moveFiles(path.src.pdfs, path.dist.pdfs);
  done();
};





function images() {
  return gulp.src( path.src.imagesFolderFiles )
    .pipe( gulp.dest( path.dist.images ) )
    .pipe( browsersync.stream() );
};

function css() {
  return gulp.src( path.src.css )
    .pipe( sourcemaps.init() )
    .pipe( sass({
      errorLogToConsole: true,
      outputStyle: 'compact'
    }))
    .on( 'error', console.error.bind( console ) )
    .pipe( autoprefixer({
      cascade: false
    }))
    .pipe( rename({ suffix: '.min' }) )
    .pipe( sourcemaps.write( './' ) )
    .pipe( gulp.dest( path.dist.css ) )
    .pipe( browsersync.stream() );
};

function js(done) {
  
  moveFiles(path.src.jsVendors, path.dist.jsVendors);

  files.js.map(function(entry) {
    return browserify({
      // make entry an array incase we have multiple later
      entries: [ path.src.jsFolder +  entry]
    })
    .transform( babelify, { presets: ['@babel/preset-env'] } )
    .bundle()
    .pipe( source( entry ) )
    // .pipe( rename({ extname: '.min.js' }) )
    .pipe( buffer() )
    .pipe( sourcemaps.init({ loadMaps: true }) )
    // .pipe( uglify() )
    .pipe( sourcemaps.write( './' ) )
    .pipe( gulp.dest( path.dist.js ) )
    .pipe( browsersync.stream() );
  });
  done();
};

function watch_files(){
  gulp.watch(path.src.css,    css);
  gulp.watch(path.src.html,   html);
  gulp.watch(path.src.images, images);
  gulp.watch(path.src.js,     js);
  gulp.watch(path.src.pdfs,   pdfs);
  
};

gulp.task('css', css);
gulp.task('html', html);
gulp.task('js', js);
gulp.task('images', images);
gulp.task('pdfs', pdfs);
gulp.task('clean', clean);

gulp.task('default', gulp.parallel(css, html, images, js, pdfs) );
gulp.task('watch', gulp.parallel(watch_files, browser_sync));







