Vue.component("portfolio-skills", {
    template: `
    <ul class="c-list--unstyled nh-skills-list u-flex u-flex-wrap">
     <li 
      v-for="skill in skills" 
      :class="'nh-bg-' + skill[0].color"
      class="c-badge u-medium">
        {{ skill[0].name }}
      </li>
    </ul>
  `,
    data() {
        return {
            skills: [
                [{ name: "HTML", color: "lime" }],
                [{ name: "CSS3", color: "pink" }],
                [{ name: "SASS", color: "orange" }],
                [{ name: "Responsive", color: "blue-pale" }],
                [{ name: "Git", color: "lime" }],
                [{ name: "JavaScript", color: "pale-blue" }],
                [{ name: "jQuery", color: "pink-light" }],
                [{ name: "Gulp", color: "lime-light" }],
                [{ name: "Jekyll", color: "pink" }],
                [{ name: "Twig", color: "coral" }],
                [{ name: "Pattern Libraries", color: "orange" }],
                [{ name: "PHP", color: "coral" }],
                [{ name: "Wordpress", color: "lime-light" }],
                [{ name: "Squarespace", color: "blue-pale" }],
                [{ name: "Bootstrap", color: "orange" }],
                [{ name: "Salesforce", color: "pink" }],
                [{ name: "Sitecore", color: "lime" }],
                [{ name: "Magnolia", color: "pink-light" }],
                [{ name: "Deploybot", color: "lime-light" }],
            ],
        };
    },
});


Vue.component("portfolio-examples", {
    template: `
    <ul class="c-list c-list--unstyled ">
      <li 
        :id="projectIds[index]"
        class="work-item"
        v-for="(example, index) in examples" 
        >

        <div class="o-grid__cell">
          <h4 class="c-heading">{{ example.name }}  </h4>
        </div>
        <div class="o-grid o-grid--small-full o-grid--medium-full">
          <div class="o-grid__cell">
            <span v-html="example.description"></span>
            <ul class="c-list c-list--unstyled c-list--comma ">
             <li 
              v-for="(exampleSkill, index) in example.skills" 
              class="c-list__item"
              >{{ exampleSkill }}</li>
            </ul>
          </div>
          
          <div class="o-grid__cell">
            <a :href="example.url" target="_blank"><img :src="example.image" class="o-image" :alt="example.name"></a>
            <img :src="example.image2" class="o-image" :alt="example.name">
          </div>
        </div>
      </li>
    </ul>
  `,
    data() {
        return {
            examples: [
                {
                    name: "Avon",
                    url: "http://avon.nathanhiemstra.com/",
                    image: "_lib/images/portfolio/sm/avon.jpg",
                    description: `<p>We designed, developed, and executed Avon’s first on-time product launch in 15 years. </p> 
              <p>I lead a team of developers who delivered a <a href="http://avon.nathanhiemstra.com/" target="_blank" class="c-link">style guide and pattern library</a>, built on Bootstrap, and customized using Atomic Design methodology. Our team directed Avon IT to implement this complete redesign into their system. </p>`,
                    skills: [
                        "HTML",
                        "SASS",
                        "CSS",
                        "JavaScript",
                        "jQuery",
                        "Git",
                        "Gulp",
                        "Twig",
                        "Deploybot",
                    ],
                },
                {
                    name: "Nissan Commercial Vehicles",
                    url: "https://www.nissanusa.com/nissan-commercial-vehicles.html",
                    image: "_lib/images/portfolio/sm/nissan.jpg",
                    description: ` <p>I led a team of developers in model-year updates, implementation of new features, and continual maintenance of Nissan Commercial Vehicle’s website. </p>
              <p>An initiative I took was creating a command line script that reduced the multiple-day task of exporting 2500 images for the model year update under one hour. This handled variances of four vehicle models, interior and exterior shots, 10 colors, and multiple sizes— all for their 8-frame, <a href="https://www.nissanusa.com/vehicles/commercial/nv-cargo/gallery.html" target="_blank" class="c-link">360 degree rotating image</a>.</p>`,
                    skills: [
                        "HTML",
                        "SASS",
                        "CSS",
                        "JavaScript",
                        "jQuery",
                        "Git",
                        "Bash",
                    ],
                },
                {
                    name: "Van Buren Youth Camp",
                    url: "http://vbyc.org",
                    image: "_lib/images/portfolio/sm/vbyc.jpg",
                    description: `<p>I led UI, UX, and the copywriting team on this pro bono redesign. We developed a Wordpress theme and built the full solution on Bootstrap.</p>`,
                    skills: [
                        "HTML",
                        "SASS",
                        "CSS",
                        "Bootstrap",
                        "PHP",
                        "Wordpress",
                        "JavaScript",
                        "jQuery",
                    ],
                },
                {
                    name: "United Airlines",
                    image: "_lib/images/portfolio/sm/united.jpg",
                    description: `<p>We migrated United Agent’s command-line interface to a modern, scalable GUI. </p>
              <p>I led a team of developers who created a style guide and module library that served as a code delivery method. We worked closely with United IT to implement this into their system.</p>`,
                    skills: [
                        "HTML",
                        "SASS",
                        "CSS",
                        "JavaScript",
                        "jQuery",
                        "Git",
                        "Jekyll",
                    ],
                },
                {
                    name: "Humana",
                    url: "https://www.humana.com/",
                    image: "_lib/images/portfolio/sm/humana.jpg",
                    description: `<p>The first responsive and accessible redesign of Humana.com.</p>
              <p>Our team worked along side Humana’s developers to implement module patterns into Humana’s Sitecore and build out pages.</p>`,
                    skills: [
                        "HTML",
                        "SASS",
                        "CSS",
                        "JavaScript",
                        "jQuery",
                        "Sitecore",
                    ],
                },
            ],
        };
    },
    computed: {
        projectIds() {
            var allIds = [];
            var thisName;
            this.examples.forEach((example) => {
                thisName = example.name;
                thisName = thisName.replace(/\s+/g, "-").toLowerCase();
                allIds.push(thisName);
            });
            return allIds;
        },
        allProjects() {
            var allNamesArr = [];
            this.examples.forEach((example) => {
                allNamesArr.push(example.name);
                allNames = allNamesArr.join(", ");
            });
            return allNames;
        },
    },
});

var app = new Vue({
    el: "#app",
});
