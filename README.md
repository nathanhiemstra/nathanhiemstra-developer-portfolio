# README #

### What is used? ###
* Gulp 4
* NPM
* ES6 

### How do I get set up? ###

* `gulp` compiles everything but does not clean first. If you add, delete, or move files, you must do `gulp clean`.
* `gulp watch` compiles (but does not clean) and starts the server. 

### Gotchas ###
* `gulp` and `gulp watch` do not clean out the dist folder. If you add, delete, or move files, run `gulp clean`.

### Who do I talk to? ###
* Nathan Hiemstra nathanhiemstra@gmail.com